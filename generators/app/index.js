var Generator = require('yeoman-generator');

module.exports = class extends Generator {

    prompting() {
      return this.prompt([{
        type    : 'input',
        name    : 'name',
        message : 'Name of your fun type',
        default : this.appname // Default to current folder name
      }]).then((props) => {
        this.props = props;
      });
    }

    writing(){
      this.fs.copyTpl(
        this.templatePath('package.json'),
        this.destinationPath('package.json'),
        { name: this.props.name }
      );
      this.fs.copy(
        this.templatePath('public'),
        this.destinationPath('public')
      );
      this.fs.copy(
        this.templatePath('src'),
        this.destinationPath('src')
      );
    }

    install(){
      this.installDependencies({
        npm: false,
        bower: false,
        yarn: true
      });
    }

    end() {
      this.log('Done!');
    }

};
