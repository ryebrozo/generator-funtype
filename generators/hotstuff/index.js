var Generator = require('yeoman-generator');

module.exports = class extends Generator {

    prompting() {
      return this.prompt([{
        type    : 'input',
        name    : 'name',
        message : 'Name of your Hotstuff variant',
        default : this.appname // Default to current folder name
      }]).then((props) => {
        this.props = props;
      });
    }

    writing(){
      this.fs.copyTpl(
        this.templatePath('package.json'),
        this.destinationPath('package.json'),
        { name: 'hotstuff_' + this.props.name }
      );
      this.fs.copy(
        this.templatePath('public'),
        this.destinationPath('public')
      );
      this.fs.copy(
        this.templatePath('src'),
        this.destinationPath('src')
      );
    }

    install(){
      this.installDependencies({
        npm: true,
        bower: false,
        yarn: false
      });
    }

    end() {
      this.log('Done!');
    }

};
