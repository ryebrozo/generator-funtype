import React, { Component } from 'react';
import Create from './scenes/Create';
import Join from './scenes/Join';
import { FunTypeCore, FunTypeCloudStorage } from 'funtype-core';
import type FunTypeCredentials from 'funtype-core';

const credentials: FunTypeCredentials = {
  id: 'Z1i8QDIGEeauFgJCcsck_Q',
  key: 'pE1iffsLo79faerj4BYK023asRwPJwQQ'
}

const props = {
  cloudStorageClient: new FunTypeCloudStorage(credentials)
}

class App extends Component {

  state: {
    previewData: ?Object
  }

  constructor(props: Props){
    super(props);
    this.state = {
      previewData: null
    }
  }

  componentWillMount(){
    let query = this.props.location.query;
    if (query.action === 'preview'){
      FunTypeCore.getJoinPreviewData(this._onGetPreviewData.bind(this));
    }
  }

  _onGetPreviewData(data: any) {
    this.setState({previewData: data});
  }

  _onClick(){
    let query = this.props.location.query;
    if (query.action === 'preview'){
      FunTypeCore.getJoinPreviewData(this._onGetPreviewData.bind(this));
    }
  }

  render() {
    let query = this.props.location.query;
    //console.log(`query = ${JSON.stringify(query)}`);
    let render = null
    switch (query.action) {
      case 'join':
        if (query.id == null) {
          console.error('id was not passed for action=join');
        }
        else {
          render = <Join id={query.id}/>
        }
        break;
      case 'preview':
        if (this.state.previewData != null){
          render = <Join previewData={this.state.previewData}/>
          // render = <p/><a href="#">Current mode is {query.action}. Click me</a>
        }
        // else {
        //   render = <div style={{marginTop:100}}><a href="#" onClick={this._onClick.bind(this)}>Current mode is {query.action}. Click me</a></div>
        // }
        break;
      default:
        render = <Create {...props} />
    }
    return render;
  }
}

export default App;
