// @flow
import React, { Component } from 'react';
import Intro from './create/Intro';

type Props = {};

export default class Create extends Component {

  state: {
    currentPage: number
  }

  constructor(props: Props){
    super(props);
    this.state = {
      currentPage: 1
    }
  }

  render() {
    let render = null;
    switch (this.state.currentPage) {
      case 1:
        render = <Intro {...this.props}/>
        break;
      default:
    }
    return render;
  }
}
