// @flow
import React, { Component } from 'react';
import Intro from './join/Intro';
import Pass from './join/Pass';

type Props = {
  id?: string,
  previewData? : Object
}
export default class Join extends Component {

  state: {
    data: ?Object,
    currentPage: number
  }

  constructor(props: Props){
    super(props);
    this.state = {
      currentPage: 1,
      data: props.previewData
    }
  }

  componentWillMount(){
    if (!this.state.data){
      let data = {
        title: 'Pass the Mug',
        backgroundImage: 'https://cdn.allwallpaper.in/wallpapers/320x568/1403/abstract-drawings-320x568-wallpaper.jpg',
        item: {
          name: 'Starbucks mug',
          imageBig: 'http://demandware.edgesuite.net/sits_pod26/dw/image/v2/AAFV_PRD/on/demandware.static/-/Sites-starbucks-master-catalog/default/dwc94cb0dd/images/upload_here/2015_Summer/04-13-15/mug_reserve_gunmtl_12_us_ko.jpg?sw=1200&sfrm=jpg',
          imageSmall: 'http://demandware.edgesuite.net/sits_pod26/dw/image/v2/AAFV_PRD/on/demandware.static/-/Sites-starbucks-master-catalog/default/dwc94cb0dd/images/upload_here/2015_Summer/04-13-15/mug_reserve_gunmtl_12_us_ko.jpg?sw=1200&sfrm=jpg'
        },
        prize: {
          name: 'Starbucks planner',
          imageBig: 'https://d8hh9kinq36uh.cloudfront.net/2016-11-17_58824.178397529.jpg',
          imageSmall: 'https://d8hh9kinq36uh.cloudfront.net/2016-11-17_58824.178397529.jpg'
        },
        source: {
          displayName: 'Starbucks',
          imageBig: 'https://upload.wikimedia.org/wikipedia/en/thumb/3/35/Starbucks_Coffee_Logo.svg/480px-Starbucks_Coffee_Logo.svg.png',
          imageSmall: 'https://upload.wikimedia.org/wikipedia/en/thumb/3/35/Starbucks_Coffee_Logo.svg/480px-Starbucks_Coffee_Logo.svg.png'
        }
      };
      this.setState({
        data: data
      });
    }
  }

  render() {
    let render = null;

    let data = this.state.data;

    if (data) {
      switch (this.state.currentPage) {
        case 1:
          render = <Intro
            backgroundImg={data.backgroundImage}
            source={data.source}
            item={data.item}
            onActionButtonClick={() => this.setState({currentPage: 2})}
          />
          break;
        case 2:
          render = <Pass
            backgroundImg={data.backgroundImage}
            item={data.item}
            prize={data.prize}
            title={data.title}
          />
          break;
        default:

      }
    }
    return render;
  }
}
