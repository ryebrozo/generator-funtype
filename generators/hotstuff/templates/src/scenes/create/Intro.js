// @flow
import React, { Component } from 'react';
import StaticCanvas from '../../components/StaticCanvas';
import Panel from '../../components/Panel';
import Button from '../../components/Button';
import { FunTypeCore, FunTypeCloudStorage } from 'funtype-core';
import './Intro.css';

const ITEM_WIDTH_PERCENTAGE = 0.65;

type Props = {
  cloudStorageClient: FunTypeCloudStorage
}

const Buttons = {
  BACKGROUND: 0,
  ITEM: 1,
  COVER: 2
}

export default class Intro extends Component {
  state : {
    selectedIndex: number,
    backgroundImg: ?string,
    itemImg: ?string,
    coverImg: ?string,
    title: ?string
  }

  constructor(props: Props) {
    super(props);
    this.state = {
      selectedIndex: -1,
      backgroundImg: null, // 'http://lorempixel.com/320/568/',
      itemImg: null, //'http://lorempixel.com/600/600/'
      coverImg: null,
      title: null
    }
  }
  _onButtonClick(index: number){
    this.setState({selectedIndex: index});
    switch (index) {
      case Buttons.BACKGROUND:
        // this.props.cloudStorageClient.insert({
        //   title: 'test',
        //   backgroundImage: 'background',
        //   itemImage: 'tesdf'
        // })
        // .catch((error) => {
        //   console.log(`Error occured while sending to cloud storage: ${error}`);
        // });
        FunTypeCore.showWebImageSelector(
          'background',
          'Select a background template',
          // [
          //   'http://lorempixel.com/320/578/sports/1/',
          //   'http://lorempixel.com/320/578/sports/2/',
          //   'http://lorempixel.com/320/578/sports/3/',
          //   'http://lorempixel.com/320/578/sports/4/',
          //   'http://lorempixel.com/320/578/sports/5/',
          //   'http://lorempixel.com/320/578/sports/6/',
          //   'http://lorempixel.com/320/578/sports/7/',
          //   'http://lorempixel.com/320/578/sports/8/',
          //   'http://lorempixel.com/320/578/sports/9/',
          //   'http://lorempixel.com/320/578/sports/10/',
          //   'http://lorempixel.com/320/578/sports/11/'
          // ],
          [
            'https://images3.pixlis.com/background-image-stripes-and-lines-seamless-tileable-232nm5.png',
            'https://images3.pixlis.com/background-image-dual-two-line-striped-seamless-tileable-beaver-victoria-234s89.png',
            'https://images1.pixlis.com/background-image-stripes-and-lines-seamless-tileable-232h4x.png',
            'https://images2.pixlis.com/background-image-dual-two-line-striped-seamless-tileable-234g7q.png'
          ],
          this._onRequestBackgroundImage.bind(this)
        );
        break;
      case Buttons.ITEM:
        FunTypeCore.showGalleryImageSelector('item', 'Select an item to pass', this._onRequestItemImage.bind(this));
        break;
      case Buttons.COVER:
        FunTypeCore.showGalleryImageSelector('cover', 'Select a cover photo', this._onRequestCoverImage.bind(this));
        break;
      default:

    }
  }

  _onRequestBackgroundImage(key: string, backgroundImg: string) {
    this.setState({backgroundImg: backgroundImg});
  }

  _onRequestItemImage(key: string, itemImg: string) {
    this.setState({itemImg: itemImg});
  }

  _onRequestCoverImage(key: string, coverImg: string) {
    this.setState({coverImg: coverImg});
    FunTypeCore.showTitleInput(this._onRequestTitle.bind(this));
  }

  _onRequestTitle(title: string) {
    this.setState({title: title});

    this.props.cloudStorageClient.insert({
      title: title,
      backgroundImage: this.state.backgroundImg
    })
    .then((response) => {
      FunTypeCore.endContentEditorWithOutput(title, this.state.coverImg, {
        title: title,
        backgroundImage: this.state.backgroundImg,
        item: {
          imageBig: this.state.itemImg
        }
      });
    })
    .catch((error) => {
      console.log(`Error occured while sending to cloud storage: ${error}`);
    });
  }

  render() {
    var width = window.innerWidth;
    var height = window.innerHeight;

    let background = null;
    let itemImage = null;
    let cover = null;


    if (this.state.selectedIndex !== Buttons.COVER){
      if (this.state.backgroundImg != null){
        background = <img src={this.state.backgroundImg} className='background' alt='background'/>
      }
      else {
        background = <StaticCanvas width={width} height={height}/>;
      }
      if (this.state.itemImg != null){
        itemImage = <img src={this.state.itemImg} className='itemz' width={width * ITEM_WIDTH_PERCENTAGE} height={width * ITEM_WIDTH_PERCENTAGE} alt='item'/>;
      }
    }
    else {
      if (this.state.coverImg == null){
        if (this.state.backgroundImg != null){
          background = <img src={this.state.backgroundImg} className='background' alt='background'/>
        }
        else {
          background = <StaticCanvas width={width} height={height}/>;
        }
        if (this.state.itemImg != null){
          itemImage = <img src={this.state.itemImg} className='itemz' width={width * ITEM_WIDTH_PERCENTAGE} height={width * ITEM_WIDTH_PERCENTAGE} alt='item'/>;
        }
      }
      else {
        cover = <img src={this.state.coverImg} className='background' alt='background'/>
      }
    }
    return (
      <div className='container'>
        {background}
        {cover}
        {itemImage}
        <Panel id="content" title="Content" backgroundColor="rgba(38, 38, 38, 0.8)" width="200px" titleColor="White" className="uppercase lighter">
          <Button title="Background" isActive={ this.state.selectedIndex === 0 } onClick={() => { this._onButtonClick(0); }}/>
          <Button title="Item" isActive={ this.state.selectedIndex === 1 } onClick={() => { this._onButtonClick(1); }}/>
          <Button title="Cover" isActive={ this.state.selectedIndex === 2 } onClick={() => { this._onButtonClick(2); }}/>
        </Panel>
      </div>
    );
  }
}
