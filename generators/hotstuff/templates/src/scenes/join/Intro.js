// @flow
import React, { Component } from 'react';
import './Intro.css';

export default class Intro extends Component {
  render() {
    return (
      <div className='container'>
        <img className='background' src={this.props.backgroundImg} alt='background'/>
        <div className='header'>
          <img className='avatar' src={this.props.source.imageBig} alt='avatar'/>
          <div className='headerInfo'>
            <div className='displayName'>{this.props.source.displayName}</div>
            <div className='caption'>posted this Hot Stuff</div>
          </div>
        </div>
        <img className='item' src={this.props.item.imageBig} alt='item' />
        <div className='footer'>
          <div className='footer1'>Lucky Joiners can win Prizes!</div>
          <div className='footer2'>Pass the hot stuff to see if you won a prize</div>
        </div>
        <a className='actionButton' href='#' onClick={this.props.onActionButtonClick}>Pass</a>
      </div>
    );
  }
}
