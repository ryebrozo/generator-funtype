// @flow
import React, { Component } from 'react';
import { FunTypeCore } from 'funtype-core';
import './Pass.css';

type Props = {

}

type Friend = {
  id: string,
  avatarBig: string,
  avatarSmall: string
}

export default class Pass extends Component {

  state: {
    inviteContent: Array<mixed>,
    hasInvite: boolean,
    friends: Array<Friend>
  }
  constructor(props: Props){
    super(props);
    this.state = {
      inviteContent: ['?','?','?','?','?','?'],
      hasInvite: false,
      friends: []
    }
  }

  _generateInviteContent(friends: Array<Friend>){
    let inviteContent = this.state.inviteContent;
    for (var i=0; i<6; i++){
      let friend = friends[i];
      if (friend != null){
        inviteContent[i] = <img className='inviteAvatar hidden' src={friend.avatarSmall} alt=''/>;
      }
      else {
        inviteContent[i] = '?';
      }
    }
    this.setState({
      inviteContent: inviteContent,
      hasInvite: true
    })
  }

  _onRequestFriends(key: string, friends: Array<Friend>) {
    this._generateInviteContent(friends);
  }
  _onActionButtonClick(){
    if (!this.state.hasInvite) {
      FunTypeCore.showFriendsSelector('test', this._onRequestFriends.bind(this));
    }
    else {
      let random = Math.floor((Math.random() * 2) + 1);
      switch (random) {
        case 1:
          FunTypeCore.endWithOutput('You passed an item in', this.props.item.imageBig);
          break;
        default:
          FunTypeCore.endWithOutput('You won a prize in', this.props.prize.imageBig);
      }
    }
  }

  render() {
    let actionButtonTitle: string;
    if (this.state.hasInvite) actionButtonTitle = 'Pass'; else actionButtonTitle = 'Select';
    return (
      <div className='passContainer'>
        <img className='background' src={this.props.backgroundImg} alt='background'/>
        <div className='passHeader'>
          <img className='passItem' src={this.props.item.imageBig} alt='item'/>
          <div className='title'>B: {this.props.title}</div>
        </div>
        <div className='passCircle'>
          <a className='passInvitee inviteOne' href='#'>{this.state.inviteContent[0]}</a>
          <a className='passInvitee inviteTwo' href='#'>{this.state.inviteContent[1]}</a>
          <a className='passInvitee inviteThree' href='#'>{this.state.inviteContent[2]}</a>
          <a className='passInvitee inviteFour' href='#'>{this.state.inviteContent[3]}</a>
          <a className='passInvitee inviteFive' href='#'>{this.state.inviteContent[4]}</a>
          <a className='passInvitee inviteSix' href='#'>{this.state.inviteContent[5]}</a>
        </div>
        <div className='footer'>
          <div className='footer1'>Random 6</div>
          <div className='footer2'>Tap button to get six random users</div>
        </div>
        <a className='actionButton' href='#' onClick={() => this._onActionButtonClick()}>{actionButtonTitle}</a>
      </div>
    );
  }
}
